package com.adsagrotech;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class Splash extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Thread timer = new Thread() {
            public void run() {
                /**
                 Records the time for which the splash screen is to be shown
                 **/

                try {
                    sleep(2000);


                } catch (InterruptedException e) {
                    e.printStackTrace();

                } finally {

                    /**
                     Opening the GetStarted Activty
                     */
                    Intent intent = new Intent(Splash.this,MainActivity.class);
                    startActivity(intent);


                }
            }

        };

        timer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        /** End the splash Screen for the current activity lifecycle*/
        finish();
    }
}
